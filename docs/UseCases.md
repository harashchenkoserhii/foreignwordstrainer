# Foreign Words Trainer

# Use Cases:

## Login / Sign Up
Before using the service, users should sign up first. Users are required to provide their email address and create a password. Initially, we will only support email addresses, but in the future, we plan to integrate a social login feature (optional).

## Add New Word to User's Dictionary
Users enter the desired word in the input field and click the "Search" button. If the word is found, the user can add it to their dictionary.

## Select Words to Train
Users select words they want to train and click the "Train Words" button. If the users select specific words, they will train only those; otherwise, they will train all words from their dictionary.

## Check Progress
Users can track their progress in training words and the total number of words they have learned.

## Compete with Other Users
Every user earns points for completed training sessions, allowing them to see other users' points and compete with them.
