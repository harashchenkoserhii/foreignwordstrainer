# Foreign Words Trainer

## App goals

1. I aim to help users improve their vocabulary and, in the future, add functionality for learning grammar, pronunciation, and writing skills. The goal is to provide interactive exercises in a game-like manner.

2. The app enables anyone to learn foreign words, irrespective of age or gender, and it is entirely free.

3. For the development:
   - Web Application: ASP.NET
   - Mobile Application: Xamarin (in future)
   - Cloud Services: Leveraging cloud services (in future)
   - Orchestration: Utilizing Kubernetes (in future)

4. I aspire to implement a microservices architecture, although initially, it will be a monolithic application.