# Foreign Words Trainer

## Description
Foreign Words Trainer is a web application which can help you efficiently learn and practice new foreign words. For more information you can visit docs folder, where you will find all needed information about this application.

## Tech stack
C#, ASP.NET MVC, SQL Server, HTML, CSS, Bootstrap, JavaScript, Cookies, JWT and many others.

## Design (will be added soon)
-- 2-4 Images or GIF from application --

## Features (in work)
- **User-Friendly Interface:** Intuitive design for a seamless learning experience.
- **Flashcards:** Engage in vocabulary drills with interactive flashcards.
- **Pronunciation Guide:** Learn the correct pronunciation with audio examples.
- **Progress Tracking:** Monitor your progress and see your improvements over time.
- **Quizzes and Games:** Reinforce your learning through fun and challenging quizzes and games.

## How to Use
1. Register of Sign in.
2. Add words to your dictionary.
3. From your dictionary select words you want to train.
4. Train selected words. 

## Installation
### Prerequisites (will be added soon)
### Dependencies (will be added soon)

## License
This project is licensed under the MIT License - see the [LICENSE.md](docs/LICENSE.md) file for details.

## Project status
Continuously developing.
