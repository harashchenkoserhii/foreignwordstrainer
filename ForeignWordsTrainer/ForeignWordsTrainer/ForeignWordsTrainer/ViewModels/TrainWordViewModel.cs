﻿using ForeignWordsTrainer.Models.LearnModels;

namespace ForeignWordsTrainer.ViewModels
{
    public class TrainWordViewModel
    {
        public List<WordModel> Words { get; set; }
        public List<WordModel> SelectedWords {  get; set; }
        public LearnUserModel LearnUser { get; set; }

    }
}
