﻿using ForeignWordsTrainer.Models.LearnModels;

namespace ForeignWordsTrainer.ViewModels
{
    public class DictionaryViewModel
    {
        public string SearchTerm { get; set; }

        public WordModel WordInformation { get; set; }

        public LearnUserModel LearnUser { get; set; }
    }
}
