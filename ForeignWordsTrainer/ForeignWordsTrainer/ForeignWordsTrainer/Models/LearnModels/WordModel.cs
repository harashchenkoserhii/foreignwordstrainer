﻿using Microsoft.AspNetCore.Mvc;

namespace ForeignWordsTrainer.Models.LearnModels
{
    public class WordModel
    {
        public int WordId { get; set; }

        public string Word { get; set; }

        public string TranslUkr { get; set; }

        public string TranslRus { get; set; }
    }
}
