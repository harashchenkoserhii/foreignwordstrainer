﻿using ForeignWordsTrainer.Data.WordsLearningDb.Models;

namespace ForeignWordsTrainer.Models.LearnModels
{
    public class LearnUserModel
    {
        public string? Name { get; set; }

        public IList<WordModel> Words { get; set; } = new List<WordModel>();
    }
}
