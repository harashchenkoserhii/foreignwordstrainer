using ForeignWordsTrainer.Data;
using ForeignWordsTrainer.Data.AuthSystemDb;
using ForeignWordsTrainer.Models.AuthModels;
using ForeignWordsTrainer.Utils.AuthJwt;
using ForeignWordsTrainer.Utils;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.CookiePolicy;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.IdentityModel.Tokens;
using System.Security.Claims;
using System.Text;
using ForeignWordsTrainer.Services;

var builder = WebApplication.CreateBuilder(args);

// Add variables to access connection strings.
var wordsLearningDbConnectionString = builder.Configuration.GetConnectionString("WordsLearningDbConnection") ?? throw new InvalidOperationException("Connection string 'WordsLearningDbConnection' not found.");
var authSystemDbConnectionString = builder.Configuration.GetConnectionString("AuthSystemDbConnection") ?? throw new InvalidOperationException("Connection string 'AuthSystemDbConnection' not found.");

builder.Services.AddDbContext<AuthSystemDbContext>(options =>
    options.UseSqlServer(authSystemDbConnectionString));

builder.Services.AddDbContext<WordsLearningDbContext>(options =>
    options.UseSqlServer(wordsLearningDbConnectionString));

builder.Services.AddIdentity<AuthUser, IdentityRole>()
    .AddEntityFrameworkStores<AuthSystemDbContext>() 
    .AddDefaultTokenProviders();

builder.Services.AddAuthentication(options => 
{
    options.DefaultAuthenticateScheme = JwtBearerDefaults.AuthenticationScheme;
    options.DefaultSignInScheme = JwtBearerDefaults.AuthenticationScheme;
    options.DefaultChallengeScheme = JwtBearerDefaults.AuthenticationScheme;
})
    .AddJwtBearer(options =>
    {
        options.RequireHttpsMetadata = true;
        options.SaveToken = true;
        options.Events = new JwtBearerEvents
        {
            OnMessageReceived = context => 
            {

                if (context.Request.Cookies.ContainsKey("X-Access-Token"))
                {
                    context.Token = context.Request.Cookies["X-Access-Token"];
                }

                return Task.CompletedTask;
            }
        };
        options.TokenValidationParameters = new TokenValidationParameters
        {
            ValidateIssuerSigningKey = true,
            IssuerSigningKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(builder.Configuration["Jwt:SecretKey"])),
            ClockSkew = TimeSpan.Zero,
            ValidateLifetime = true,
            ValidateIssuer = true,
            ValidIssuer = builder.Configuration["Jwt:Issuer"],
            ValidateAudience = true,
            ValidAudience = builder.Configuration["Jwt:Audience"],
        };
    });

builder.Services.AddAuthorization(builder =>
{
    builder.AddPolicy("role", policyBuilder =>
    {
        policyBuilder.RequireAuthenticatedUser()
            .AddAuthenticationSchemes(JwtBearerDefaults.AuthenticationScheme)
            .RequireClaim("RoleUserPolicy", "RegularUser");
    });
});

builder.Services.AddScoped<JwtTokenGenerator>(); 

builder.Services.AddScoped<WordService>();
builder.Services.AddScoped<WordMappingService>();
builder.Services.AddScoped<UserWordsService>();

builder.Services.AddControllersWithViews();

builder.Services.AddRazorPages();

var app = builder.Build();

// Configure the HTTP request pipeline.
if (!app.Environment.IsDevelopment())
{
    app.UseExceptionHandler("/Home/Error");
    // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
    app.UseHsts();
}

app.UseCookiePolicy(new CookiePolicyOptions
{
    MinimumSameSitePolicy = SameSiteMode.Strict,
    HttpOnly = HttpOnlyPolicy.Always,
    Secure = CookieSecurePolicy.Always
});

app.UseHttpsRedirection();

app.UseRouting();

app.MapRazorPages();

app.UseAuthentication();
app.UseAuthorization();

app.UseStatusCodePages();
app.UseStaticFiles();

app.MapControllerRoute(
    name: "default",
    pattern: "{controller=Home}/{action=Index}/{id?}");

app.Run();
