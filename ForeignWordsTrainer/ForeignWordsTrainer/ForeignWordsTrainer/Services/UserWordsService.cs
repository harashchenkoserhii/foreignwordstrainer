﻿using ForeignWordsTrainer.Data;
using ForeignWordsTrainer.Data.WordsLearningDb.Models;
using ForeignWordsTrainer.Models.LearnModels;
using ForeignWordsTrainer.Services.Status;
using Microsoft.EntityFrameworkCore;
using System.Linq;
using System.Security.Claims;

namespace ForeignWordsTrainer.Services
{
    public class UserWordsService
    {
        private readonly WordsLearningDbContext _dbContext = default!;
        private readonly WordMappingService _wordMappingService;

        public UserWordsService(WordsLearningDbContext dbContext, WordMappingService wordMappingService)
        {
            _dbContext = dbContext;
            _wordMappingService = wordMappingService;
        }
        public IOperationStatus AddWordToUserDictionary(string userId, WordDalModel newWord)
        {
            var user = _dbContext.UsersList.Include(u => u.Words).FirstOrDefault(u => u.Id == userId);
            var word = _dbContext.WordsList.Include(u => u.Users).FirstOrDefault(u => u.Id == newWord.Id);

            if (user != null)
            {
                if (user.Words == null)
                {
                    user.Words = new List<WordDalModel>();
                }

                if (!user.Words.Any(w => w.Word == newWord.Word))
                {
                    user.Words.Add(word);
                    word.Users.Add(user);

                    _dbContext.SaveChanges();
                    return new OperationStatus(true, "Word added to user's dictionary successfully");
                }
                else
                {
                    return new OperationStatus(false, "Word is already in user's dictionary");
                }
            }
            else
            {
                return new OperationStatus(false, "User not found");
            }
        }

        public IOperationStatus DeleteWordFromUserDictionary(string userId, WordDalModel wordToDelete)
        {
            var user = _dbContext.UsersList.Include(u => u.Words).FirstOrDefault(u => u.Id == userId);
            var word = _dbContext.WordsList.Include(u => u.Users).FirstOrDefault(u => u.Id == wordToDelete.Id);

            if (user != null && word != null)
            {
                var wordToRemove = user.Words.FirstOrDefault(w => w.Id == word.Id);

                if (wordToRemove != null)
                {
                    user.Words.Remove(wordToRemove);
                    word.Users.Remove(user);

                    _dbContext.SaveChanges();
                    return new OperationStatus(true, "Word deleted successfully");
                }
                else
                {
                    return new OperationStatus(false, "Word not found in user's dictionary");
                }
            }
            else
            {
                return new OperationStatus(false, "User or word not found");
            }
        }
        public LearnUserModel GetUserWords(string userIdentifier)
        {
            var userId = userIdentifier;
            UserDalModel dalUser = null;
            LearnUserModel learnUser = null;

            if (userId != null)
            {
                dalUser = _dbContext.UsersList.Include(u => u.Words).SingleOrDefault(u => u.Id == userId);
                

                if (dalUser != null && dalUser.Words != null && dalUser.Words.Any())
                {
                    var words = dalUser.Words.Select(w => _wordMappingService.MapToWordModel(w)).ToList();

                    learnUser = new LearnUserModel()
                    {
                        Name = dalUser.Name,
                        Words = words
                    };
                }
                else
                {
                    learnUser = new LearnUserModel()
                    {
                        Name = dalUser.Name,
                        Words = new List<WordModel>()
                    };
                }
            }
            return learnUser;
        }
    }
}
