﻿using ForeignWordsTrainer.Data.WordsLearningDb.Models;
using ForeignWordsTrainer.Models.LearnModels;

namespace ForeignWordsTrainer.Services
{
    public class WordMappingService
    {
        public WordModel MapToWordModel(WordDalModel dalModel)
        {
            return new WordModel
            {
                WordId = dalModel.Id,
                Word = dalModel.Word,
                TranslUkr = dalModel.TranslUkr,
                TranslRus = dalModel.TranslRus
            };
        }
        public List<WordModel> MapToWordModel(List<WordDalModel> wordDalModels)
        {
            return wordDalModels.Select(MapToWordModel).ToList();
        }
        public WordDalModel MapToWordDalModel(WordModel model)
        {
            return new WordDalModel
            {
                Id = model.WordId,
                Word = model.Word,
                TranslUkr = model.TranslUkr,
                TranslRus = model.TranslRus
            };
        }
    }

}
