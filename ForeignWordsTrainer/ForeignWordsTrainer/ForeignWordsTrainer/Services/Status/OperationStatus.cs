﻿namespace ForeignWordsTrainer.Services.Status
{
    public class OperationStatus : IOperationStatus
    {
        public bool Success { get; }
        public string Message { get; }

        public OperationStatus(bool success, string message)
        {
            Success = success;
            Message = message;
        }
    }
}
