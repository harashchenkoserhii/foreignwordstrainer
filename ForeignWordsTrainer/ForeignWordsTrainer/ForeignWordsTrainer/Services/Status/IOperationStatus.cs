﻿namespace ForeignWordsTrainer.Services.Status
{
    public interface IOperationStatus
    {
        bool Success { get; }
        string Message { get; }
    }
}
