﻿using ForeignWordsTrainer.Data;
using ForeignWordsTrainer.Data.WordsLearningDb.Models;
using ForeignWordsTrainer.Models.LearnModels;

namespace ForeignWordsTrainer.Services
{
    public class WordService
    {
        private readonly WordsLearningDbContext _dbContext;

        public WordService(WordsLearningDbContext dbContext)
        {
            _dbContext = dbContext;
        }
        public List<WordDalModel> GetAllWordsFromDatabase()
        {
            var wordsWithTranslations = _dbContext.WordsList.Select(uw => uw).ToList(); 

            return wordsWithTranslations;
        }
    }
}
