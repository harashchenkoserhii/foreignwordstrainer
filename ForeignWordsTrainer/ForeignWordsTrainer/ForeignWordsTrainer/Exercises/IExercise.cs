﻿namespace ForeignWordsTrainer.Exercises
{
    public interface IExercise
    {
        public bool CheckAnswer();
    }
}
