﻿using Microsoft.AspNetCore.Mvc;

namespace ForeignWordsTrainer.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class StartController : Controller
    {
        public IActionResult StartPage()
        {
            return View("~/Views/Shared/AppStart.cshtml");
        }
    }
}
