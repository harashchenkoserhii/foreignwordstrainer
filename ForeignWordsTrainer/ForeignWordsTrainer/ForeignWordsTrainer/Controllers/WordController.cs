﻿using ForeignWordsTrainer.Data;
using ForeignWordsTrainer.Data.WordsLearningDb.Models;
using ForeignWordsTrainer.Models;
using ForeignWordsTrainer.Models.LearnModels;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace ForeignWordsTrainer.Controllers
{
    [Authorize("role")]
    [Route("api/[controller]")]
    [ApiController]
    public class WordController : Controller
    {
        private readonly WordsLearningDbContext _dbContext;

        public WordController(WordsLearningDbContext dbContext)
        {
            _dbContext = dbContext;
        }
        private void LoadUserWords(UserDalModel user)
        {
            _dbContext.Entry(user).Collection(u => u.Words).Load(); // have to manualy load words collection for current user.
        }
        private UserDalModel GetUserById(string userId)
        {
            return _dbContext.UsersList.SingleOrDefault(u => u.Id == userId);
        }

        [HttpPost("{userId}")]
        public IActionResult AddNewWord(string userId, [FromBody] string newWord)
        {
            string newWordLowerCase = newWord.ToLower();
            // Check if the word exists in the "words_table"
            var existingWord = _dbContext.WordsList.SingleOrDefault(w => w.Word == newWordLowerCase);

            if (existingWord == null)
            {
                return BadRequest($"There is no '{newWord}' in the global dictionary.");
            }

            // *TODO: think about this functionality,
            // coz only authorized users has access to wordController
            // Check if the user exists in the "users_table"
            var currentUser = GetUserById(userId);

            if (currentUser == null)
            {
                return BadRequest($"User not found.");  
            }                                           

            LoadUserWords(currentUser);

            // Check if the word is already associated with the user
            if (currentUser.Words.Contains(existingWord))
            {
                return BadRequest($"Word '{newWordLowerCase}' already exist in {currentUser.Name}'s dictionary");
            }

            currentUser.Words.Add(existingWord);

            _dbContext.SaveChanges();

            return Ok($"Word {newWordLowerCase} added to {currentUser.Name}'s dictionary successfully.");
        }

        [HttpGet("{userId}")]
        public IActionResult GetWords(string userId)
        {
            // *
            var currentUser = GetUserById(userId);
            // Check if the user exists in the "users_table".
            if (currentUser == null)
            {
                return BadRequest($"User not found.");
            }

            LoadUserWords(currentUser);

            // Extract words from the Words collection.
            if (currentUser.Words == null || !currentUser.Words.Any())
            {
                return Ok($"{currentUser.Name}'s dictionary is empty.");
            }

            // Return collection with WordWithTranslation objects.
            var wordsWithTranslations = currentUser.Words.Select(uw => new WordModel
            {
                Word = uw.Word,
                TranslUkr = uw.TranslUkr,
                TranslRus = uw.TranslRus
            });

            return Ok(wordsWithTranslations);
        }

        [HttpDelete("{userId}")]
        public IActionResult DeleteWord(string userId, [FromBody] string wordToDelete)
        {
            var currentUser = GetUserById(userId);
            // Check if the user exists in the "users_table"
            if (currentUser == null)
            {
                return BadRequest($"User not found.");
            }

            LoadUserWords(currentUser);

            // Check if the word is associated with the user
            string wordToDeleteLowerCase = wordToDelete.ToLower();
            var wordToDeleteFromDb = currentUser.Words.SingleOrDefault(uw => uw.Word == wordToDelete.ToLower());

            if (wordToDeleteFromDb == null)
            {
                return BadRequest($"The word '{wordToDeleteLowerCase}' is not associated with the {currentUser.Name} user.");
            }

            currentUser.Words.Remove(wordToDeleteFromDb);
            _dbContext.SaveChanges();

            return Ok($"Word '{wordToDeleteLowerCase}' removed from {currentUser.Name}'s dictionary successfully.");
        }
    }
}
