﻿using ForeignWordsTrainer.Data;
using ForeignWordsTrainer.Data.AuthSystemDb;
using ForeignWordsTrainer.Data.WordsLearningDb.Models;
using ForeignWordsTrainer.Models.LearnModels;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System.Linq;
using System.Security.Claims;

namespace ForeignWordsTrainer.Controllers.LearnControllers
{
    [Authorize("role")]
    [Route("api/[controller]")]
    [ApiController]
    public class LearnController : Controller
    {
        private readonly WordsLearningDbContext _dbContext;
        private readonly UserManager<AuthUser> _userManager;

        public LearnController(WordsLearningDbContext context, UserManager<AuthUser> userManager)
        {
            _dbContext = context;
            _userManager = userManager;
        }
        public IActionResult Dictionary()
        {
            ViewData["Title"] = "Log in";
            return View("~/Views/Learn/Dictionary.cshtml");
        }

        [HttpGet]
        public IActionResult GetWords()
        {
            var userId = HttpContext.User.FindFirst(ClaimTypes.NameIdentifier)?.Value;
            UserDalModel dalUser = null;
            LearnUserModel learnUser = null;

            if (userId != null)
            {
                dalUser = _dbContext.UsersList.Include(u => u.Words).SingleOrDefault(u => u.Id == userId);

                if (dalUser != null && dalUser.Words != null && dalUser.Words.Any())
                {
                    var wordsWithTranslations = dalUser.Words.Select(uw => new WordModel
                    {
                        Word = uw.Word,
                        TranslUkr = uw.TranslUkr,
                        TranslRus = uw.TranslRus
                    }).ToList();

                    learnUser = new LearnUserModel()
                    {
                        Name = dalUser.Name,
                        Words = wordsWithTranslations
                    };
                }
                else
                {
                    return Ok($"{dalUser.Name}'s dictionary is empty.");
                }
            }
            return View("~/Views/Learn/Dictionary.cshtml", learnUser);
        }
        public IActionResult Exercise1()
        {
            return View();
        }
        public IActionResult Exercise2()
        {
            return View();
        }
        public IActionResult Exercise3()
        {
            return View();
        }
        public IActionResult Exercise4()
        {
            return View();
        }
        public IActionResult Exercise5()
        {
            return View();
        }
    }
}

