﻿using ForeignWordsTrainer.Data;
using ForeignWordsTrainer.Data.WordsLearningDb.Models;
using ForeignWordsTrainer.Models.LearnModels;
using ForeignWordsTrainer.Services;
using ForeignWordsTrainer.ViewModels;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System.Security.Claims;

namespace ForeignWordsTrainer.Controllers.LearnControllers
{
    [Authorize("role")]
    [Route("api/[controller]")]
    [ApiController]
    public class DictionaryController : Controller
    {
        private readonly WordsLearningDbContext _dbContext;
        private readonly Dictionary<string, WordDalModel> _wordDalModelDictionary;
        private readonly WordService _wordService;
        private readonly WordMappingService _wordMappingService;
        private readonly UserWordsService _userWordsService;
        private string UserId => GetUserId();

        public DictionaryController(WordsLearningDbContext dbContext, WordService wordService, 
            WordMappingService wordMappingService, UserWordsService userWordsService)
        {
            _dbContext = dbContext;
            _wordService = wordService;
            _wordDalModelDictionary = LoadWordsIntoDictionary();
            _wordMappingService = wordMappingService;
            _userWordsService = userWordsService;
        }
        private string GetUserId()
        {
            var userId = HttpContext.User.FindFirst(ClaimTypes.NameIdentifier)?.Value;
            return userId;
        }

        [HttpPost("Search")]
        public IActionResult Search([FromForm]string searchTerm = "")
        {
            if (string.IsNullOrWhiteSpace(searchTerm))
            {
                var viewModel = new DictionaryViewModel
                {
                    SearchTerm = searchTerm,
                    WordInformation = new WordModel(),
                    LearnUser = _userWordsService.GetUserWords(UserId)
                };
                return View("~/Views/Learn/Dictionary.cshtml", viewModel);
            }

            searchTerm = searchTerm.ToLower();

            if (_wordDalModelDictionary.ContainsKey(searchTerm))
            {
                var wordDalModel = _wordDalModelDictionary[searchTerm];
                var wordModel = _wordMappingService.MapToWordModel(wordDalModel);

                var viewModel = new DictionaryViewModel
                {
                    SearchTerm = searchTerm,
                    WordInformation = wordModel,
                    LearnUser = _userWordsService.GetUserWords(UserId)
                };

                return View("~/Views/Learn/Dictionary.cshtml", viewModel);
            }
            else
            {
                var viewModel = new DictionaryViewModel
                {
                    SearchTerm = searchTerm,
                    WordInformation = new WordModel(),
                    LearnUser = _userWordsService.GetUserWords(UserId)
                };
                return View("~/Views/Learn/Dictionary.cshtml", viewModel);
            }
        }

        [HttpPost("ProcessAddWordToUserDictionary")]
        public IActionResult ProcessAddWordToUserDictionary([FromForm] WordModel wordModel)
        {
            //if (!ModelState.IsValid)
            //{
            //    return View("YourOriginalView", wordModel); TODO: adjust later
            //}

            //if (string.IsNullOrWhiteSpace(searchTerm))
            //{
            //    var viewModel = new DictionaryViewModel
            //    {
            //        SearchTerm = searchTerm,
            //        WordInformation = new WordModel(),
            //        LearnUser = _userWordsService.GetUserWords(UserId)
            //    };
            //    return View("~/Views/Learn/Dictionary.cshtml", viewModel);
            //}

            var userId = UserId;
            var dalModel = _wordMappingService.MapToWordDalModel(wordModel);

            var wordAddingStatus = _userWordsService.AddWordToUserDictionary(userId, dalModel);

            if (wordAddingStatus.Success)
            {
                return RedirectToAction("Dictionary", "Dictionary");
            }
            else
            {
                if (wordAddingStatus.Message == "User not found")
                {
                    return RedirectToAction("Index", "Home");
                }
                else if (wordAddingStatus.Message == "Word is already in user's dictionary")
                {
                    return RedirectToAction("Dictionary", "Dictionary");
                }
                else
                {
                    return View("Error", wordAddingStatus);
                }
            }
        }

        [HttpPost("ProcessRemoveWordFromUserDictionary")]
        public IActionResult ProcessRemoveWordFromUserDictionary([FromForm] WordModel wordModel)
        {
            var userId = UserId;
            var dalModel = _wordMappingService.MapToWordDalModel(wordModel);

            var wordRemovingStatus = _userWordsService.DeleteWordFromUserDictionary(userId, dalModel);

            if (wordRemovingStatus.Success)
            {
                return RedirectToAction("Dictionary", "Dictionary");
            }
            else
            {
                if (wordRemovingStatus.Message == "User not found")
                {
                    return RedirectToAction("Index", "Home");
                }
                else if (wordRemovingStatus.Message == "Word not found in user's dictionary")
                {
                    return RedirectToAction("Dictionary", "Dictionary");
                }
                else
                {
                    return View("Error");
                }
            }
        }

        private Dictionary<string, WordDalModel> LoadWordsIntoDictionary()
        {
            var wordsFromDb = _wordService.GetAllWordsFromDatabase();

            var dalWordsDictionary = wordsFromDb.ToDictionary(wm => wm.Word, wm => wm);

            return dalWordsDictionary;
        }

        [HttpGet("Dictionary")]
        public IActionResult Dictionary()
        {
            var viewModel = new DictionaryViewModel
            {
                SearchTerm = default!,
                WordInformation = new WordModel(),
                LearnUser = _userWordsService.GetUserWords(UserId),
            };

            return View("~/Views/Learn/Dictionary.cshtml", viewModel);
        }
    }
}
