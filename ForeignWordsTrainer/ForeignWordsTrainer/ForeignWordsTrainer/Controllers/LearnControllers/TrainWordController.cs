﻿using ForeignWordsTrainer.Models.LearnModels;
using ForeignWordsTrainer.Services;
using ForeignWordsTrainer.ViewModels;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System.Security.Claims;

namespace ForeignWordsTrainer.Controllers.LearnControllers
{
    [Authorize("role")]
    [Route("api/[controller]")]
    [ApiController]
    public class TrainWordController : Controller
    {
        private readonly WordService _wordService;
        private readonly UserWordsService _userWordsService;
        private readonly WordMappingService _wordMappingService;
        private string UserId => GetUserId();

        private static List<WordModel> selectedWords = new List<WordModel>();

        public TrainWordController(WordService wordService, UserWordsService userWordsService,
            WordMappingService wordMappingService)
        {
            _wordService = wordService;
            _userWordsService = userWordsService;
            _wordMappingService = wordMappingService;
        }
        private string GetUserId()
        {
            var userId = HttpContext.User.FindFirst(ClaimTypes.NameIdentifier)?.Value;
            return userId;
        }

        [HttpGet("Train")]
        public IActionResult Train()
        {
            var viewModel = new TrainWordViewModel
            {
                Words = _wordMappingService.MapToWordModel(_wordService.GetAllWordsFromDatabase()),
                SelectedWords = new List<WordModel>(),
                LearnUser = _userWordsService.GetUserWords(UserId)
            };

            return View("~/Views/Learn/TrainWord.cshtml", viewModel);
        }
        [HttpPost("AddToTrainList")]
        public IActionResult AddWordToTrainList([FromForm] WordModel selectedWord)
        {
            if (selectedWord != null || selectedWords.Contains(selectedWord))
            {
                return RedirectToAction("Train");
            }
            else
            {
                selectedWords.Add(selectedWord);
                return RedirectToAction("Train");
            }
        }

        [HttpPost("TrainWords")]
        public IActionResult TrainWords(IList<WordModel> selectedWords)
        {
            if (selectedWords == null || !selectedWords.Any())
            {
                return RedirectToAction("Train");
            }
            else
            {
                return RedirectToAction("TrainExersice");
            }

            // TODO: think where and how  clear the selectedWords collection
        }

        [HttpPost("AddWordsToDeck")]
        public ActionResult AddWordsToDeck(List<WordModel> selectedWords)
        {
            string[] selectedWords1 = Request.Form["selectedWords"]; // Get selected words from the form
                                                                    // Logic to add selected words to the user's flashcard deck (e.g., store in a database or session)
            return View(); // Redirect or return a success message
        }
    }
}

