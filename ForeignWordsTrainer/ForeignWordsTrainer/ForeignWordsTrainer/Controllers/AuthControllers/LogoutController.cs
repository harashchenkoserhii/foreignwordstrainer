﻿using ForeignWordsTrainer.Models.AuthModels;
using ForeignWordsTrainer.Utils.AuthJwt;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authentication.JwtBearer;

namespace ForeignWordsTrainer.Controllers.AuthControllers
{
    //[Authorize("role")]
    [Route("api/[controller]")]
    [ApiController]
    public class LogoutController : Controller
    {
        
        [HttpGet]
        public IActionResult Logout()
        {
            //await HttpContext.SignOutAsync(JwtBearerDefaults.AuthenticationScheme);
            Response.Cookies.Delete("X-Access-Token");
            return RedirectToAction("Index", "Home");
        }
    }
}
