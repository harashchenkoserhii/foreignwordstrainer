﻿using ForeignWordsTrainer.Data.AuthSystemDb;
using ForeignWordsTrainer.Models.AuthModels;
using ForeignWordsTrainer.Utils.AuthJwt;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;

namespace ForeignWordsTrainer.Controllers.AuthControllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class LoginController : Controller
    {
        private readonly JwtTokenGenerator _jwtTokenGenerator;
        private readonly UserManager<AuthUser> _userManager;

        public LoginController(JwtTokenGenerator jwtTokenGenerator,
            UserManager<AuthUser> userManager) 
        {
            _jwtTokenGenerator = jwtTokenGenerator;
            _userManager = userManager; 
        }

        [AllowAnonymous]
        [HttpGet]
        public IActionResult Login()
        {
            ViewData["Title"] = "Log in";
            return View("~/Views/Account/Login.cshtml");
        }

        [AllowAnonymous]
        [HttpPost]
        public async Task<IActionResult> Login([FromForm] LoginUserModel loginModel) 
        {
            var user = await _userManager.FindByNameAsync(loginModel.Email);

            if (user != null)
            {
                var isPasswordValid = await _userManager.CheckPasswordAsync(user, loginModel.Password);

                if (isPasswordValid)
                {
                    var token = _jwtTokenGenerator.GenerateJwtToken(user);

                    Response.Cookies.Append("X-Access-Token", token, new CookieOptions() { HttpOnly = true, SameSite = SameSiteMode.Strict, Secure = true });

                    return RedirectToAction("Dictionary", "Dictionary");
                }
            }

            ViewData["ErrorMessage"] = "Invalid email or password.";

            return View("~/Views/Account/Login.cshtml");
        }
    }
}
