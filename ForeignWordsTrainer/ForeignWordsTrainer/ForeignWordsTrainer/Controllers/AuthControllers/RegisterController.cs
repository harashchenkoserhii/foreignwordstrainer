﻿using ForeignWordsTrainer.Data;
using ForeignWordsTrainer.Data.AuthSystemDb;
using ForeignWordsTrainer.Data.WordsLearningDb.Models;
using ForeignWordsTrainer.Models.AuthModels;
using ForeignWordsTrainer.Models.LearnModels;
using ForeignWordsTrainer.Utils.AuthJwt;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;

namespace ForeignWordsTrainer.Controllers.AuthControllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class RegisterController : Controller
    {
        private readonly UserManager<AuthUser> _userManager;
        private readonly SignInManager<AuthUser> _signInManager;
        private readonly JwtTokenGenerator _jwtTokenGenerator;
        private readonly WordsLearningDbContext _wordsLearningDbContext;

        public RegisterController(UserManager<AuthUser> userManager,
            SignInManager<AuthUser> signInManager, JwtTokenGenerator jwtTokenGenerator,
            WordsLearningDbContext wordsLearningDbContext)
        {
            _userManager = userManager;
            _signInManager = signInManager;
            _jwtTokenGenerator = jwtTokenGenerator;
            _wordsLearningDbContext = wordsLearningDbContext;
        }

        [AllowAnonymous]
        [HttpGet]
        public IActionResult Register()
        {
            ViewData["Title"] = "Register";
            return View("~/Views/Account/Register.cshtml");
        }

        [AllowAnonymous]
        [HttpPost]
        public async Task<IActionResult> Register([FromForm] RegisterUserModel model)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState); 
            }

            var user = new AuthUser
            {
                UserName = model.Email,
                Email = model.Email,
                NormalizedEmail = model.Email.ToUpper(),
                FirstName = model.FirstName,
                LastName = model.LastName
            };

            var result = await _userManager.CreateAsync(user, model.Password); 

            if (!result.Succeeded)
            {
                foreach (var error in result.Errors)
                {
                    ModelState.AddModelError("", error.Description);
                }
                return BadRequest(ModelState);
            }

            var learnUser = new UserDalModel
            {
                Id = user.Id,
                Name = model.FirstName
            };

            _wordsLearningDbContext.UsersList.Add(learnUser);
            _wordsLearningDbContext.SaveChanges();

            var token = _jwtTokenGenerator.GenerateJwtToken(user);
            Response.Cookies.Append("X-Access-Token", token, new CookieOptions() { HttpOnly = true, SameSite = SameSiteMode.Strict, Secure = true });

            //await _signInManager.SignInAsync(user, isPersistent: true, "Bearer"); 

            return RedirectToAction("Dictionary", "Dictionary");
        }
    }
}
