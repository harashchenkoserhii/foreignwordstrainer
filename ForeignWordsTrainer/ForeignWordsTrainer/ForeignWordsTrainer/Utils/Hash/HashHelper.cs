﻿using System;
using System.Security.Cryptography;
using System.Text;

namespace ForeignWordsTrainer.Utils.Hash
{
    public class HashHelper
    {
        public const int HashSizeBytes = 24; // size in bytes
        public const int IterationsCount = 100000; // number of pbkdf2 iterations

        public static string GetPBKDF2Hash(string password, string salt)
        {
            byte[] saltBytes = Encoding.UTF8.GetBytes(salt);
            Rfc2898DeriveBytes pbkdf2 = new Rfc2898DeriveBytes(password, saltBytes, IterationsCount);
            byte[] encodedBytes = pbkdf2.GetBytes(HashSizeBytes);
            return Convert.ToBase64String(encodedBytes); // 32 chars string
        }
    }
}
