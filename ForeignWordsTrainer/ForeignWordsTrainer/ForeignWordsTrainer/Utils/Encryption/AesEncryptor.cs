﻿using System;
using System.IO;
using System.Security.Cryptography;

namespace ForeignWordsTrainer.Utils.Encryption
{
    public static class AesEncryptor
    {
        const int keySizeBytes = 32;

        public static string Encrypt(byte[] key, byte[] iv, string plainText)
        {
            if (key.Length != keySizeBytes)
            {
                throw new ArgumentException($"Incorrect key size. Should be {keySizeBytes}", nameof(key));
            }

            byte[] array;

            using (Aes aes = Aes.Create())
            {
                aes.Key = key;
                aes.IV = iv;

                ICryptoTransform encryptor = aes.CreateEncryptor(aes.Key, aes.IV);

                using (MemoryStream memoryStream = new MemoryStream())
                {
                    using (CryptoStream cryptoStream = new CryptoStream(memoryStream, encryptor, CryptoStreamMode.Write))
                    {
                        using (StreamWriter streamWriter = new StreamWriter(cryptoStream))
                        {
                            streamWriter.Write(plainText);
                        }

                        array = memoryStream.ToArray();
                    }
                }
            }

            return Convert.ToBase64String(array);
        }

        public static string Decrypt(byte[] key, byte[] iv, string cipherText)
        {
            if (key.Length != keySizeBytes)
            {
                throw new ArgumentException($"Incorrect key size. Should be {keySizeBytes}", nameof(key));
            }

            byte[] buffer = Convert.FromBase64String(cipherText);

            using (Aes aes = Aes.Create())
            {
                aes.Key = key;
                aes.IV = iv;
                ICryptoTransform decryptor = aes.CreateDecryptor(aes.Key, aes.IV);

                using (MemoryStream memoryStream = new MemoryStream(buffer))
                {
                    using (CryptoStream cryptoStream = new CryptoStream(memoryStream, decryptor, CryptoStreamMode.Read))
                    {
                        using (StreamReader streamReader = new StreamReader(cryptoStream))
                        {
                            return streamReader.ReadToEnd();
                        }
                    }
                }
            }
        }
    }
}

