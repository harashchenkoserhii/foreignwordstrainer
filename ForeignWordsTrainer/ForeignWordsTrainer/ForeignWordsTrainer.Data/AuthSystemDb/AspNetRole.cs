﻿using System;
using System.Collections.Generic;

namespace ForeignWordsTrainer.Data.AuthSystemDb;

public partial class AspNetRole
{
    public string Id { get; set; } = null!;

    public string? Name { get; set; }

    public string? NormalizedName { get; set; }

    public string? ConcurrencyStamp { get; set; }

    public virtual ICollection<AspNetRoleClaim> AspNetRoleClaims { get; set; } = new List<AspNetRoleClaim>();

    public virtual ICollection<AuthUser> Users { get; set; } = new List<AuthUser>();
}
