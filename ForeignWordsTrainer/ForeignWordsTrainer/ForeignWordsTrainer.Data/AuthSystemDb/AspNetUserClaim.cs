﻿using System;
using System.Collections.Generic;

namespace ForeignWordsTrainer.Data.AuthSystemDb;

public partial class AuthUserClaim
{
    public int Id { get; set; }

    public string UserId { get; set; } = null!;

    public string? ClaimType { get; set; }

    public string? ClaimValue { get; set; }

    public virtual AuthUser User { get; set; } = null!;
}
