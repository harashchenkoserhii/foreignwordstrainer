﻿using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;

namespace ForeignWordsTrainer.Data.AuthSystemDb;

public partial class AuthUser : IdentityUser
{
    public override string Id { get; set; } = null!;

    public string FirstName { get; set; } = null!;

    public string LastName { get; set; } = null!;

    public override string? UserName { get; set; }

    public override string? NormalizedUserName { get; set; }

    public override string? Email { get; set; }

    public override string? NormalizedEmail { get; set; }

    public override bool EmailConfirmed { get; set; }

    public override string? PasswordHash { get; set; }

    public override string? SecurityStamp { get; set; }

    public override string? ConcurrencyStamp { get; set; }

    public override string? PhoneNumber { get; set; }

    public override bool PhoneNumberConfirmed { get; set; }

    public override bool TwoFactorEnabled { get; set; }

    public override DateTimeOffset? LockoutEnd { get; set; }

    public override bool LockoutEnabled { get; set; }

    public override int AccessFailedCount { get; set; }

    public virtual ICollection<AuthUserClaim> AuthUserClaims { get; set; } = new List<AuthUserClaim>();

    public virtual ICollection<AuthUserLogin> AuthUserLogins { get; set; } = new List<AuthUserLogin>();

    public virtual ICollection<AuthUserToken> AuthUserTokens { get; set; } = new List<AuthUserToken>();

    public virtual ICollection<AspNetRole> Roles { get; set; } = new List<AspNetRole>();
}
