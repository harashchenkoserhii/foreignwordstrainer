﻿using System;
using System.Collections.Generic;

namespace ForeignWordsTrainer.Data.AuthSystemDb;

public partial class AuthUserLogin
{
    public string LoginProvider { get; set; } = null!;

    public string ProviderKey { get; set; } = null!;

    public string? ProviderDisplayName { get; set; }

    public string UserId { get; set; } = null!;

    public virtual AuthUser User { get; set; } = null!;
}
