﻿using System;
using System.Collections.Generic;

namespace ForeignWordsTrainer.Data.AuthSystemDb;

public partial class AuthUserToken
{
    public string UserId { get; set; } = null!;

    public string LoginProvider { get; set; } = null!;

    public string Name { get; set; } = null!;

    public string? Value { get; set; }

    public virtual AuthUser User { get; set; } = null!;
}
