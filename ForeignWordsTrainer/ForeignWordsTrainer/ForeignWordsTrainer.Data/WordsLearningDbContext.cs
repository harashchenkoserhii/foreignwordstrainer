﻿using ForeignWordsTrainer.Data.WordsLearningDb.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ForeignWordsTrainer.Data
{
    public partial class WordsLearningDbContext : DbContext
    {
        public IConfiguration _configuration { get; }
        public WordsLearningDbContext() {}

        public WordsLearningDbContext(DbContextOptions<WordsLearningDbContext> options, IConfiguration configuration)
            : base(options)
        {
            _configuration = configuration;
        }

        public virtual DbSet<UserDalModel> UsersList { get; set; }

        public virtual DbSet<WordDalModel> WordsList { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
            => optionsBuilder.UseSqlServer(_configuration.GetConnectionString("WordsLearningDbConnection"));

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<UserDalModel>(entity =>
            {
                entity.ToTable("users_table");

                entity.Property(e => e.Id)
                    .HasMaxLength(68)
                    .HasColumnName("id");
                entity.Property(e => e.Name)
                    .HasMaxLength(25)
                    .HasColumnName("name");

                entity.HasMany(d => d.Words).WithMany(p => p.Users)
                    .UsingEntity<Dictionary<string, object>>(
                        "UsersWordsDalModel",
                        r => r.HasOne<WordDalModel>().WithMany()
                            .HasForeignKey("WordId")
                            .HasConstraintName("FK_users_words_table_words"),
                        l => l.HasOne<UserDalModel>().WithMany()
                            .HasForeignKey("UserId")
                            .HasConstraintName("FK_users_words_table_users"),
                        j =>
                        {
                            j.HasKey("UserId", "WordId");
                            j.ToTable("users_words_table");
                            j.IndexerProperty<string>("UserId")
                                .HasMaxLength(68)
                                .HasColumnName("user_id");
                            j.IndexerProperty<int>("WordId").HasColumnName("word_id");
                        });
            });

            modelBuilder.Entity<WordDalModel>(entity =>
            {
                entity.HasKey(e => e.Id).HasName("PK__word__3213E83FC4D7EB6B");

                entity.ToTable("words_table");

                entity.Property(e => e.Id).HasColumnName("id");
                entity.Property(e => e.TranslRus)
                    .HasMaxLength(100)
                    .HasColumnName("translRus");
                entity.Property(e => e.TranslUkr)
                    .HasMaxLength(100)
                    .HasColumnName("translUkr");
                entity.Property(e => e.Word)
                    .HasMaxLength(50)
                    .HasColumnName("word");
            });

            OnModelCreatingPartial(modelBuilder);
        }

        partial void OnModelCreatingPartial(ModelBuilder modelBuilder);
    }
}