﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ForeignWordsTrainer.Data.WordsLearningDb.Models
{
    public partial class WordDalModel
    {
        public int Id { get; set; }

        public string? Word { get; set; }

        public string? TranslUkr { get; set; }

        public string? TranslRus { get; set; }

        public virtual ICollection<UserDalModel> Users { get; set; } = new List<UserDalModel>();
    }
}
