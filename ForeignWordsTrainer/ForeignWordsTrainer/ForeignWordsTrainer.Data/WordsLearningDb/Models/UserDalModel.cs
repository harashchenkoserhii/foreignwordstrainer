﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ForeignWordsTrainer.Data.WordsLearningDb.Models
{
    public partial class UserDalModel
    {
        public string Id { get; set; } = null!;

        public string? Name { get; set; }

        public virtual ICollection<WordDalModel> Words { get; set; } = new List<WordDalModel>();
    }
}
